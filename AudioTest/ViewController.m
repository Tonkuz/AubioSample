//
//  ViewController.m
//  AudioTest
//
//  Created by Tony Thomas on 19/01/16.
//  Copyright © 2016 Tony Thomas. All rights reserved.
//

#import "ViewController.h"
#import "EZAudio/EZAudio.h"
@import aubio;
@interface ViewController ()<EZMicrophoneDelegate>{
    
    fvec_t* samples;
    fvec_t* outOnset;
    uint_t read;
    smpl_t testThreshold ;
    aubio_onset_t*  onset ;
    int onsetCount;
}

@property (nonatomic, strong)   EZMicrophone* mic;
@property (nonatomic)   int64_t audioPresentationValue;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       NSError* error;
    // Do any additional setup after loading the view, typically from a nib.
    [[AVAudioSession sharedInstance]  setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    self.mic = [[EZMicrophone alloc]initWithMicrophoneDelegate:self];
    Float32 preferredBufferTime = 512 / 44100.0;
 
    [[AVAudioSession sharedInstance] setPreferredIOBufferDuration:preferredBufferTime error:&error];
    [self initAubio];
    [self.mic startFetchingAudio];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initAubio{
    samples = new_fvec(512);
    outOnset = new_fvec(1);
    read = 0;
    onset = new_aubio_onset("default", 512, 512, 44100);
    testThreshold = 2.5;
    aubio_onset_set_threshold(onset, testThreshold);
}

-(void)    microphone:(EZMicrophone*)microphone
        hasBufferList:(AudioBufferList*)bufferList
       withBufferSize:(UInt32)bufferSize
 withNumberOfChannels:(UInt32)numberOfChannels{
    for (int i =0 ; i<512; i++) {
        fvec_set_sample(samples, *(float*)(bufferList->mBuffers[0].mData), i);
    }
    aubio_onset_do(onset, samples, outOnset);
    
    if (fvec_get_sample(outOnset, 0)) {
        NSLog(@"%f",aubio_onset_get_last_s(onset));
        onsetCount++;
    }
    
 }

@end
